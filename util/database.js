const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize('ingeTP', 'postgres', '18101999',{
    host: 'localhost',
    port:  5432,
    dialect: 'postgres',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },

});

module.exports = sequelize;
