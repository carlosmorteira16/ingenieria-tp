const productos = require('../models/productos');
const {check, validationResult} = require('express-validator');

    /*listado*/
    exports.listado = async (req, res) => {
        const products = await productos.findAll();
        res.json(products)
    };

    /*visualizacion*/
    exports.visualizacion = async (req, res) => {
         
        const products = await productos.findByPk(req.params.id);
        if(!products){
            res.status(404).send({
                status: 'error',
                message: `Producto con el id ${req.params.id} no se encuentra`
            })
        }else{
            res.json(products)
        }
       
    };

    /*crear*/
    exports.crear =  async(req, res) => {
        const products = await productos.create(req.body)
        res.json(products)
    };

    /*edicion*/
    exports.edicion = async(req, res, next) => {  
        console.log('Un usuario ingreso al servicio del url editar producto');
        try {
            console.log( req.params.id)
            const id = req.params.id
            const findProduct = await productos.findOne({
                where: {
                    idProducto: id
                }
            })
            if(!findProduct){
                res.status(404).send({
                    status: 'error',
                    message: `Producto con el id ${id} no se encuentra`
                })
            }  
            const updateProducto = await productos.update({
                nombre : req.body.nombre,
                codigo : req.body.codigo,
                modelo : req.body.modelo,
                familia : req.body.familia,
                stock :  req.body.stock
                },
                { 
                    where : {
                        idProducto : id,
                       
                    }
                });


            if(!updateProducto){
                res.status(400).send({
                    status: 'error',
                    message: `Error al actualizar el producto id ${id}`
                })
            }

            res.status(200).send({
                status: 'success',
                data: updateProducto,
                message: `Producto editado`
            })
            
        }catch(error){
            console.log(error)
        }
    };

    /*borrar*/
    exports.delete = async (req, res) => {
        try{
            const {id} = req.params;
            const findProduct = await productos.findByPk(id)
            if(!findProduct){
                res.status(404).send({
                    status: 'error',
                    message: `El producto con el id ${id} no se encuentra`
                })
            }
            const deleteProduct = await findProduct.destroy()
            if(!deleteProduct){
                res.status(503).send({
                    status: 'error',
                    message:`Hubo un problema al querer eliminar el producto ${id}` 
                })
            }
            res.status(200).send({
                status: 'success',
                message: `El producto ${id} fue eliminado exitosamente `
            })
        } catch(error){
            console.log(`Error: ${error}`)
        }
    };