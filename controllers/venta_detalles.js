const ventas = require('../models/venta_detalle');
const { Op, where } = require("sequelize");

    /*listado*/
    exports.listado = async (req, res) => {
        console.log('Un usuario ingreso al servicio del url sequelize /ventas_detalles listar');
        whereCondition = {};
        whereCondition['nro_factura'] = req.params.id
        const venta = await ventas.findAll({where: whereCondition,});
        if(!venta || venta.length ==[]){
            res.status(404).send({
                status: 'error',
                message: `venta detalle no se encuentra`
            })
        }else{
            res.json(venta)
        }
    };

    /*visualizacion*/
    exports.visualizacion = async (req, res) => {  
        console.log('Un usuario ingreso al servicio del url """visualizar""" ver ventas_detalles');
        console.log(req.params)
        whereCondition = {};
        if(req.params.id){
            whereCondition = {
                [Op.and]: [
                    { 'nro_factura' :   req.params.id },
                    { 'id' : req.params.id },
                ]
            };  
        }
       
        const venta = await ventas.findAll({where: whereCondition,});
        if(!venta || venta.length ==[]){
            res.status(404).send({
                status: 'error',
                message: `venta detalle no se encuentra`
            })
        }else{
            res.json(venta)
        }
        console.log(1222)
    };

    /*crear*/
    exports.crear = async (req, res) => {
        console.log('Un usuario ingreso al servicio del url /ventas_detalles crear');
        console.log(req.params.id)
        console.log(req.body)
        const nuevoParametro ={ 
            nro_factura:req.params.id,
            idProducto: req.body.idProducto,
            cantidad: req.body.cantidad,
        };
        const products = await ventas.create(nuevoParametro)
        res.json(products)
    };
    
    /*edicion*/
    exports.edicion = async (req, res) => {
        console.log('Un usuario ingreso al servicio del url editar ventas_detalles');
        try {
            const id = req.params.id
            const findVenta_detalle = await ventas.findOne({
                where: {
                    id: id
                }
            })
            if(!findVenta_detalle){
                res.status(404).send({
                    status: 'error',
                    message: `venta_detalle con el id ${id} no se encuentra`
                })
            }  
            const updateVenta_detalle = await ventas.update({
                nro_factura : req.body.nro_factura,
                idProducto : req.body.idProducto,
                cantidad: req.body.cantidad,
                },
                { 
                    where : {
                        id : id
                    }
                });


            if(!updateVenta_detalle){
                res.status(400).send({
                    status: 'error',
                    message: `Error al actualizar el producto id ${id}`
                })
            }

            res.status(200).send({
                status: 'success',
                message: `venta_detalle editada`,
                data: updateVenta_detalle
            })
            
        }catch(error){
            console.log(error)
        }
    };

    /*borrar*/
    exports.delete = async (req, res) => {
        try{

            const {id} = req.params;
            const findVentas = await ventas.findByPk(id)
            if(!findVentas){
                res.status(404).send({
                    status: 'error',
                    message: 'El producto con el id ${id} no se encuentra'
                })
            }
            const deleteVentas = await findVentas.destroy()
            if(!deleteVentas){
                res.status(503).send({
                    status: 'error',
                    message: `Hubo un problema al querer eliminar la venta ${id}`
                })
            }
            res.status(200).send({
                status: 'success',
                message: `La venta n° ${id} fue eliminado exitosamente`
            })
        } catch(error){
            console.log(`Error: ${error}`)
        }
    };

