const ventas = require('../models/ventas');

    /*listado*/
    exports.listado = async (req, res) => {
        const venta = await ventas.findAll();
        res.json(venta)
    };

    /*visualizacion*/
    exports.visualizacion = async (req, res) => {
        const venta = await ventas.findByPk(req.params.id);
        if(!venta){
            res.status(404).send({
                status: 'error',
                message: `venta con el nro_factura ${req.params.id} no se encuentra`
            })
        }else{
            res.json(venta)
        }
       
    };

    /*crear*/
    exports.crear =  async(req, res) => {
        const venta = await ventas.create(req.body)
        res.json(venta)
    };

    /*edicion*/
    exports.edicion = async(req, res, next) => {  
        console.log('Un usuario ingreso al servicio del url editar producto');
        try {
            const id = req.params.id
            const findVenta = await ventas.findOne({
                where: {
                    nro_factura: id
                }
            })
            if(!findVenta){
                res.status(404).send({
                    status: 'error',
                    message: `Producto con el id ${id} no se encuentra`
                })
            }  
            const updateVenta = await ventas.update({
                nro_factura : req.body.nro_factura,
                fecha : req.body.fecha,
                total: req.body.total,
                iva10 : req.body.iva10,
                iva5 : req.body.iva5,
                },
                { 
                    where : {
                        nro_factura : id,
                        
                    }
                });


            if(!updateVenta){
                res.status(400).send({
                    status: 'error',
                    message: `Error al actualizar el producto id ${id}`
                })
            }

            res.status(200).send({
                status: 'success',
                data: updateVenta,
                message: `venta editada`
            })
            
        }catch(error){
            console.log(error)
        }

    };

    /*borrar*/
    exports.delete = async (req, res) => {
        try{
            const {id} = req.params;
            const findVentas = await ventas.findByPk(id)
            if(!findVentas){
                res.status(404).send({
                    status: 'error',
                    message: 'El producto con el id ${id} no se encuentra'
                })
            }
            const deleteVentas = await findVentas.destroy()
            if(!deleteVentas){
                res.status(503).send({
                    status: 'error',
                    message: `Hubo un problema al querer eliminar la venta ${id}`
                })
            }
            res.status(200).send({
                status: 'success',
                message: `La venta n° ${id} fue eliminado exitosamente`
            })
        } catch(error){
            console.log(`Error: ${error}`)
        }
    };

