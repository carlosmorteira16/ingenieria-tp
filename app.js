const http = require('http');
//importamos express, luego de haberlo instalado
const express = require('express');
//creamos una aplicacion express nueva
const app = express();

// agregamos la libreria de templates que vamos a utilizar
  app.use(express.static(__dirname + '/public'));
    app.set('view engine', 'ejs');
    app.set('views', 'views');
    app.use(express.urlencoded({
    extended: true
  }));

  const path = require('path');

  app.use(express.static(path.join(__dirname, 'public')));

  const ventas = require('./routes/ventas');
  app.use(ventas);

  const ventas_detalles = require('./routes/ventas_detalle');
  app.use(ventas_detalles);

  const productos = require('./routes/productos');
  app.use(productos);


  app.use('/bienvenida', (request, response, next) => {
    console.log('Un usuario ingreso al servicio del url /bienvenida');

    response.send('<h1>Bienvenido usuario/a</h1>');
  });

  app.use((request, response, next) => {
    console.log('Ruta desconocida');
    response.status(404).send('<h1>Error 404 - Not found</h1>');
  });

//creamos el servidor web, pero sin ejecutarlo
const server = http.createServer(app);

//iniciamos el servidor web, esta linea hace que se ejecute realmente en base a la configuracion anterior
server.listen(3000);
