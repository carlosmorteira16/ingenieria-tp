const express = require('express');
const router = express.Router();
const controlador = require('../controllers/productos');

    router.get('/productos', controlador.listado);
    router.get('/productos/:id', controlador.visualizacion)
    router.post('/productos',controlador.crear);
    router.put('/productos/:id', controlador.edicion);
    router.delete('/productos/:id', controlador.delete);

module.exports = router;
