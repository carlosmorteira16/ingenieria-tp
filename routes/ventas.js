const express = require('express');
const router = express.Router();
const controlador = require('../controllers/ventas');

    router.get('/ventas', controlador.listado);
    router.get('/ventas/:id',controlador.visualizacion)
    router.post('/ventas',controlador.crear);
    router.put('/ventas/:id',controlador.edicion);
    router.delete('/ventas/:id',controlador.delete);

module.exports = router;
