const express = require('express');
const router = express.Router();
const deta = require('../models/relaciones');
const controlador = require('../controllers/venta_detalles');


    router.get('/ventas/:id/ventas_detalles', controlador.listado);
    router.get('/ventas/:id/ventas_detalles/:id',controlador.visualizacion);
    router.post('/ventas/:id/ventas_detalles',controlador.crear);
    router.put('/ventas/:id/ventas_detalles/:id',controlador.edicion);
    router.delete('/ventas/:id/ventas_detalles/:id',controlador.delete);

module.exports = router;

