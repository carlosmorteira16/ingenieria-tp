const sequelize = require('../util/database');
const venta = require('./ventas');
const Producto = require('./productos');
const venta_detalle = require('./venta_detalle');

Producto.hasMany(venta_detalle, {
    foreignKey: 'idProducto', //nombre del FK
    sourceKey: 'idProducto' //nombre del PK en la tabla
});


venta_detalle.belongsTo(Producto, {
    foreignKey: 'idProducto', //nombre del FK
    sourceKey: 'id'
});

venta.hasMany(venta_detalle, {
    foreignKey: 'nro_factura', //nombre del FK
    sourceKey: 'nro_factura' //nombre del PK en la tabla
});

venta_detalle.belongsTo(venta, {
    foreignKey: 'nro_factura', //nombre del FK
    sourceKey: 'nro_factura'
});

sequelize.sync({ alter: true }).then(r => {
    console.log('Todos los modelos han sido sincronizados ');
});

module.exports =  {venta_detalle, venta};
module.exports =  {venta_detalle, Producto}
