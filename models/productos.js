
const sequelize = require('../util/database');
const { DataTypes } = require('sequelize');

const productos =  sequelize.define('producto', {
    // Model attributes are defined here
    idProducto: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: false,

        validate: {
           
            is: function(value){
                const expRe1 = /[0-9]/;
                const expRe2 = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                if(String(this.getDataValue('nombre')).match(expRe1) || String(this.getDataValue('nombre')).match(expRe2)){
                    throw new Error ('solo se puede introducir letras')
                }
            },
            
            len: [3, 100],
            customValidator(value) {
                const nom = this.getDataValue('nombre');
                if (nom.length < 3) {
                    throw new Error ('nombre menor a 3 digitos')
                }   
            }
        },
    },
    codigo: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
           
            is: function(value){
                const expRe1 = /[0-9]/;
                const expRe2 = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                if(String(this.getDataValue('codigo')).match(expRe1) || String(this.getDataValue('codigo')).match(expRe2)){
                    throw new Error ('solo se puede introducir letras')
                }
            },
            
            len: [3, 100],
            customValidator(value) {
                const nom = this.getDataValue('codigo');
                if (nom.length < 3) {
                    throw new Error ('codigo menor a 3 digitos')
                }   
            }
        },
    },
    modelo: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    familia: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    stock: {
        type: DataTypes.INTEGER,
        allowNull: false,
    }, 
    
    
    createdAt: DataTypes.DATE,
    Dias: {
        type: DataTypes.VIRTUAL,
        get: function() {
            const days = Math.floor((Date.now() - this.get('createdAt'))/(60*60*1000*24) );
            return this.getDataValue('createdAt') ? days : "--"  
        }
    },
    updatedAt: DataTypes.DATE,
},
{
    tableName: 'producto',
   
    // Other model options go here
});


productos.sync({ alter: true }).then(res => {
    console.log("The table for the Producto model was just (re)created!");
});

module.exports = productos ;




