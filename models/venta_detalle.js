const sequelize = require('../util/database');
const { DataTypes } = require('sequelize');

 const ventas_detalles =  sequelize.define('venta_detalle', {
        // Model attributes are defined here
        nro_factura: {
            type: DataTypes.BIGINT,
            allowNull: false,
        },
        idProducto: {
            type: DataTypes.BIGINT,
            allowNull: false,
            validate: {
                is: function(value){
                    const expRe1 = /^[a-z]+$/i;
                    const expRe2 = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                    if(String(this.getDataValue('idProducto')).match(expRe1) || String(this.getDataValue('idProducto')).match(expRe2)){
                        throw new Error ('solo se puede introducir numeros')
                    }
                },
            },
        },
        cantidad: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                is: function(value){
                    const expRe1 = /^[a-z]+$/i;
                    const expRe2 = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                    if(String(this.getDataValue('cantidad')).match(expRe1) || String(this.getDataValue('cantidad')).match(expRe2)){
                        throw new Error ('solo se puede introducir numeros')
                    }
                },
            },
        },
        
     
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
    },
    {
        tableName: 'venta_detalle',
       
        // Other model options go here
    });
    
    
    ventas_detalles.sync({ alter: true }).then(res => {
        console.log("The table for the Producto model was just (re)created!");
    });
    
module.exports = ventas_detalles ;
