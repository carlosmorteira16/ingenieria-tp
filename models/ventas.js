const sequelize = require('../util/database');
const { DataTypes } = require('sequelize');

const ventas =  sequelize.define('venta', {
    // Model attributes are defined here
    nro_factura: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    fecha: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    total: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
           
            is: function(value){
                const expRe1 = /^[a-z]+$/i;
                const expRe2 = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                if(String(this.getDataValue('total')).match(expRe1) || String(this.getDataValue('total')).match(expRe2)){
                    throw new Error ('solo se puede introducir numeros')
                }
            },
            
           
        },
    },
    iva10: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            is: function(value){
                const expRe1 = /^[a-z]+$/i;
                const expRe2 = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
                if(String(this.getDataValue('total')).match(expRe1) || String(this.getDataValue('total')).match(expRe2)){
                    throw new Error ('solo se puede introducir numeros')
                }
            },
        },
    },
    iva5: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },

    
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
},
{
    tableName: 'ventas',
   
    // Other model options go here
});


ventas.sync({ alter: true }).then(res => {
    console.log("The table for the ventas model was just (re)created");
});

module.exports = ventas ;



